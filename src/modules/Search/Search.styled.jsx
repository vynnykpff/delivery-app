import styled from "@emotion/styled";
import {Link} from "react-router-dom";

export const SearchBlock = styled.div`
  position: absolute;
  border-radius: 20px;
  width: 100%;
  max-height: 200px;
  right: 0;
  top: 60px;
  background-color: #ccc;
  overflow: auto;
  padding: 20px;
`;

export const ProductCard = styled(Link)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;

export const ProductCardName = styled.p`
`;

export const ProductCardImage = styled.img`
  border-radius: 15px;
  background-color: #fff;
  width: 50px;
  height: 50px;
`;
