import {ProductCard, ProductCardImage, ProductCardName, SearchBlock} from "./Search.styled.jsx";
import NoData from "../../shared/components/NoData/NoData.jsx";
import {FaHistory} from "react-icons/fa";
import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {requestMenu} from "../../redux/menu/menu.slice.js";

const Search = ({products, inputValue, isActive}) => {
	const dispatch = useDispatch();
	const filteredProducts = products.filter(product => {
		return product.name.toLowerCase().includes(inputValue.toLowerCase());
	})

	useEffect(() => {
		dispatch(requestMenu());
	}, [])

	return (
		<SearchBlock onMouseLeave={() => isActive(false)}>
			{filteredProducts.length
				? filteredProducts.map(product => (
					<ProductCard key={product.id}>
						<ProductCardName>{product.name}</ProductCardName>
						<ProductCardImage src={product.image} alt="product image"/>
					</ProductCard>
				))
				: <NoData icon={<FaHistory/>}/>
			}
		</SearchBlock>
	);
};

export default Search;