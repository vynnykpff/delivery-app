import {CartIcon, CountBuys, SearchBlock, SearchIcon, SearchInput, SearchWrapper} from "./SearchField.styled.jsx";
import {shoppingCart} from "../../../../../../shared/constants/routes.js";
import {NavItem} from "../../NavBar.styled.jsx";
import {useDispatch, useSelector} from "react-redux";
import {Divider} from "../../../../../../shared/styles/GlobalStyles.jsx";
import {setValue} from "../../../../../../redux/filter/filter.slice.js";
import {useEffect, useState} from "react";
import Search from "../../../../../Search/Search.jsx";
import {requestMenu} from "../../../../../../redux/menu/menu.slice.js";

const SearchField = () => {
	const {count} = useSelector(state => state.count);
	const dispatch = useDispatch();
	const [search, setSearch] = useState(false);
	const {arrayMenu} = useSelector(state => state.menu)
	const [inputValue, setInputValue] = useState("");
	const [products, setProducts] = useState([]);

	const getProducts = () => {
		arrayMenu.map(item => {
			const data = Object.values(item);
			const newData = data.map(item => Object.values(item));
			const newValue = newData.flat(2);
			setProducts(newValue)
		});
	}

	useEffect(() => {
		dispatch(requestMenu())
	}, [dispatch]);

	useEffect(() => {
		getProducts();
	}, [arrayMenu]);

	return (
		<SearchBlock>
			<SearchIcon/>
			<SearchInput
				onFocus={() => setSearch(true)}
				onChange={e => setInputValue(e.target.value)}
				placeholder="Search"
			/>

			{search && products.length ? <Search isActive={setSearch} products={products} inputValue={inputValue}/> : ""}

			<SearchWrapper>
				<Divider/>
				<NavItem className="shoppingCart" to={shoppingCart}>
					<CountBuys>{count}</CountBuys>
					<CartIcon/>
				</NavItem>
			</SearchWrapper>
		</SearchBlock>
	);
};

export default SearchField;