import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {requestProduct, setProductInfo} from "../../../../../../redux/menu/menu.slice.js";
import Loader from "../../../../../../shared/components/Loader/Loader.jsx";
import setNumberFormat from "../../../../../../shared/utils/setNumberFormat.js";

const CardDescription = () => {
	const dispatch = useDispatch();
	const {name, selectValue, id} = useParams();
	const {product} = useSelector(state => state.menu);

	useEffect(() => {
		dispatch(setProductInfo({name, selectValue, id}));
		dispatch(requestProduct());
	}, [dispatch])

	return (
		<>
			{product.length ?
				<div>
					<h2>{product[0].name}</h2>
					<img src={product[0].image} alt="image"/>
					<p>{setNumberFormat("ru-RU", {style: "currency", currency: "UAH"}, product[0].price)}</p>
					<p>{product[0].description}</p>
				</div>
				: <Loader/>
			}
		</>
	);
};

export default CardDescription;