import History from "../../modules/History/History.jsx";
import {ClearButton} from "../../modules/History/History.styled.jsx";
import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {login, profile} from "../../shared/constants/routes.js";
import {deleteCollection} from "../../shared/utils/deleteCollection.js";
import {getCollection} from "../../shared/utils/firebase/getCollection.js";
import NoData from "../../shared/components/NoData/NoData.jsx";
import {FaHistory} from "react-icons/fa";
import {getCookie} from "../../shared/utils/cookies/getCookie.js";

const HistoryPage = () => {
	const [collectionOrders, setCollectionOrders] = useState([]);
	const navigate = useNavigate();
	const isAuthValue = getCookie('isAuth');

	useEffect(() => {
		const getHistory = async () => {
			const data = await getCollection("orders");
			if (data) {
				setCollectionOrders(data);
			} else {
				navigate(login);
			}
		}

		getHistory();

	}, [navigate]);

	const handleResetHistory = async () => {
		await deleteCollection(["orders"]);
		navigate(profile);
	};

	return (
		<>
			{isAuthValue ?
				collectionOrders.length ?
					<>
						<ClearButton onClick={handleResetHistory}>Clear history</ClearButton>
						<History collectionOrders={collectionOrders}/>
					</>
					:
					<NoData icon={<FaHistory/>}/>
				: navigate(login)
			}
		</>
	);
};

export default HistoryPage;
