import {createSlice} from "@reduxjs/toolkit";

const filterSlice = createSlice({
	name: "filter",
	initialState: {
		inputValue: ""
	},
	reducers: {
		setValue: (state, action) => {
			state.inputValue = action.payload;
		},
	},
});

export const {setValue} = filterSlice.actions;
export default filterSlice.reducer;


