import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {API} from "../../api/index.js";

const createAndCheckError = (response, message) => {
	if (!response.status === 200) {
		throw new Error(message);
	}
};

export const requestMenu = createAsyncThunk(
	'menu/requestMenu',
	async function (_, {rejectWithValue}) {
		try {
			const response = await API.shops.getMenu();
			createAndCheckError(response, "Can't get menu. Server error");
			return response;
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);

export const requestProduct = createAsyncThunk(
	'menu/requestProduct',
	async function (_, {rejectWithValue}) {
		try {
			const response = await API.shops.getMenu();
			createAndCheckError(response, "Can't get menu. Server error");
			return response;
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);

const menuSlice = createSlice({
	name: 'menu',
	initialState: {
		arrayMenu: [],
		product: [],
		shopName: "",
		selectCategory: "",
		productId: "",
		status: null,
		error: null,
	},
	reducers: {
		setProductInfo: (state, action) => {
			return {
				...state,
				shopName: action.payload.name,
				selectCategory: action.payload.selectValue,
				productId: action.payload.id,
			}
		}
	},
	extraReducers: {
		[requestMenu.pending]: state => {
			state.status = false;
			state.error = null;
		},
		[requestMenu.fulfilled]: (state, action) => {
			state.status = true;
			state.arrayMenu = action.payload;
		},
		[requestMenu.rejected]: state => {
			state.error = 'rejected';
		},
		[requestProduct.pending]: state => {
			state.status = false;
			state.error = null;
		},
		[requestProduct.fulfilled]: (state, action) => {
			const data = action.payload[0][state.shopName][state.selectCategory].filter(item => item.id === state.productId);
			return {
				...state,
				product: data,
				status: true,
			}
		},
		[requestProduct.rejected]: state => {
			state.error = 'rejected';
		},
	}
})

export const {setProductInfo} = menuSlice.actions;

export default menuSlice.reducer;


