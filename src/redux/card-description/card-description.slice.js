import {createSlice} from "@reduxjs/toolkit";

const cardDescription = createSlice({
	name: "cardDescription",
	initialState: {
		image: "",
		price: 0,
		id: null,
		shops: "",
	},
	reducers: {
		setCardDescription: (state, action) => {
			return {
				...state,
				image: action.payload,
			}
		},
	},
});

export const {setCardDescription} = cardDescription.actions;
export default cardDescription.reducer;


